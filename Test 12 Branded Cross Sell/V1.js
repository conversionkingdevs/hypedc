var dl = dataLayer[1]['ecommerce']['detail']['products'][0]['category'];
if (dl.indexOf('Sneakers') > -1) {

    /* RM Williams 

    '0' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/2cbc98650354489c99b7882231a422ea.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/aed61a0dbdec46ffa2a2a63d041660f8.png',
            "img":      '//cdn.optimizely.com/img/6092490016/2df0c9c5724d4913bb2a6c45a352160b.jpg',
            "name":     'Suede Protector',
            "price":    '<span class="price product-price"><span class="price-dollars">$17</span><span class="price-cents">.99</span></span>',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290802,
          	"cartid": '51SP000110-NOC',
            "href":     'https://www.hypedc.com/rm-williams-suede-protector.html',
            "text": '<span>The R.M. Williams Suede Protector</span><span> is designed to protect fabrics from water and</span><span> stains during wear.</span>',
          	'bodyclass': 'opt71-large'
        },
        '1' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/727439b9f7e9485d80310070538b98ad.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/ce4014c8f476499db2b139d22a7f7fad.png',
            "img":      '//cdn.optimizely.com/img/6092490016/27bba3e378be45fa8d8b8e07f0071410.jpg',
            "name":     'Suede Cleaner',
            "price":    '<span class="price product-price"><span class="price-dollars">$17</span><span class="price-cents">.99</span></span>',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290800,
          	"cartid": '50SC000110-NOC',
            "href":     'https://www.hypedc.com/rm-williams-suede-cleaner.html',
            "text": '<span>The R.M. Williams Suede Cleaner.</span><span> A non-abrasive, leave-in spray that renews</span><span> aniline materials.</span>',
          	'bodyclass': 'opt71-large'
        },
        '2' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/de632fa32143487c882ae2c280678bdd.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/7c57d0174b1145e8aa427e5644b7b574.png',
            "img":      '//cdn.optimizely.com/img/6092490016/3e3d80418f0042f2818908dba68f3470.jpg',
            "name":     'Stockman\'s Boot Polish',
            "price":    '<span class="price product-price"><span class="price-dollars">$17</span><span class="price-cents">.99</span></span>',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290798,
          	"cartid": '44BP410107-NEUT',
            "href":     'https://www.hypedc.com/stockmans-boot-polish-chestnut.html',
            "text": '<span>The R.M. Williams range of Boot Polishes</span><span> come in a variety of colours to suit the</span><span> right pair of boots.</span>',
          	'bodyclass': 'opt71-small'
        },
        '3' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/c63e154642f24292a412918de758288c.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/c31757617de34b7ca2f5c4d6d4dd6db6.png',
            "img":      '//cdn.optimizely.com/img/6092490016/517aa33a53c04103ba9f52200172ef3e.jpg',
            "name":     'Stockman\'s Boot Polish',
            "price":    '<span class="price product-price"><span class="price-dollars">$17</span><span class="price-cents">.99</span></span>',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290794,
          	"cartid": '44BP130107-TAN',
            "href":     'https://www.hypedc.com/stockmans-boot-polish-tan.html',
            "text": '<span>The R.M. Williams range of Boot Polishes</span><span> come in a variety of colours to suit the</span><span> right pair of boots.</span>',
          	'bodyclass': 'opt71-small'
        },
        '4' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/898e22bc25db48ecb172c91e2f1267f7.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/e1cccdeabae141269173e62ddc14ed5f.png',
            "img":      '//cdn.optimizely.com/img/6092490016/3eb26c42553143c39d37056b905af01b.jpg',
            "name":     'Stockman\'s Boot Polish',
            "price":    '<span class="price product-price"><span class="price-dollars">$17</span><span class="price-cents">.99</span></span>',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290792,
          	"cartid": '44BP020107-BLK',
            "href":     'https://www.hypedc.com/stockmans-boot-polish-black.html',
            "text": '<span>The R.M. Williams range of Boot Polishes</span><span> come in a variety of colours to suit the</span><span> right pair of boots.</span>',
          	'bodyclass': 'opt71-small'
        },
        '6' : {
            "brand":    'RM Williams',
            "img-desktop":      '//cdn.optimizely.com/img/6092490016/aca5497b949a4ddda271826824f572ad.png',
            "img-mobile":      '//cdn.optimizely.com/img/6092490016/e4ca635f90ee45ab85322fce2ea16a93.png',
            "img":      '//cdn.optimizely.com/img/6092490016/de1a65d93fe6482681f6f2cded16ef2e.jpg',
            "name":     'Stockman\'s Boot Polish',
            "value":    '17<sup>.99</sup>',
            "sAttr":    4974,
            "pID":      290796,
          	"cartid": '44BP270107-NAT',
            "href":     'https://www.hypedc.com/stockmans-boot-polish-natural.html',
            "text": '<span>The R.M. Williams range of Boot Polishes</span><span> come in a variety of colours to suit the</span><span> right pair of boots.</span>',
          	'bodyclass': 'opt71-small'
        },  
    */

    var products = {
        '0': {
            "brand": 'Jason Markk',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/cd9731293e01480f8c8c08cd4a61468f.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/216019496d6744a0bcd1e02c01a02e51.png',
            "img": '//cdn.optimizely.com/img/6092490016/5dd359f81589495b8d416bcd86551292.jpg',
            "name": 'Essential Kit',
            "value": '29<sup>.99</sup>',
            "sAttr": 2622,
            "pID": 254076,
            "cartid": '000024458',
            "href": 'https://www.hypedc.com/jason-markk-kit-assorted.html',
            "text": '<span>The Jason Markk Essential Kit</span><span> is your first stop for</span><span> shoe cleaning solutions.</span>',
            'bodyclass': 'opt71-small'
        },
        '1': {
            "brand": 'Jason Markk',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/b09b034a83514a2590a3bc3fcfcbbbde.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/d894eec91c5c446996e79a2575f94051.png',
            "img": '//cdn.optimizely.com/img/6092490016/fb4c11d4383c4d32b6224bf67aaee34c.jpg',
            "name": '8 OZ. Premium Shoe Cleaner',
            "value": '29<sup>.99</sup>',
            "sAttr": 2622,
            "pID": 254078,
            "cartid": '000024459',
            "href": 'https://www.hypedc.com/jason-markk-8oz-assorted.html',
            "text": '<span>Jason Markk\'s best-selling</span><span> shoe cleaner is a gentle foaming solution</span><span> that cleans and conditions.</span>',
            'bodyclass': 'opt71-small'
        },
        '2': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/c57cad7e535748109315cf20b4ec3f86.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/6d9ad3cd68884c3e9f8cde9c3ac07e09.png',
            "img": '//cdn.optimizely.com/img/6092490016/aaa06b8a624e46dd838047d1b393732c.jpg',
            "name": 'Sneaker Protector',
            "value": '14<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290857,
            "cartid": 'SPZ-001-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-sneaker-protector.html',
            "text": '<span>The Sneaker Lab Sneaker Protector</span><span> provides a dirt and stain repellent layer</span><span> that is spritzed onto footwear uppers.</span>',
            'bodyclass': 'opt71-small'
        },
        '3': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/631f1c13d3b04a42ac7024380d28361a.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/88f8e1a0934d40a88185f60bc0bc2b16.png',
            "img": '//cdn.optimizely.com/img/6092490016/b4491acc9dff459d9c6c32b7234e75c3.jpg',
            "name": 'Premium Kit',
            "value": '34<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290780,
            "cartid": 'SP4-001-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-premium-kit.html',
            "text": '<span>An extension of the Basic Kit,</span><span> Elevate the shoe cleaning process with</span><span> Sneaker LAB\'s Premium Kit.</span>',
            'bodyclass': 'opt71-small'
        },
        '4': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/8c69fc897dcc4053a8c667d57168d414.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/be9b2015c87b4c868edd3869f719350d.png',
            "img": '//cdn.optimizely.com/img/6092490016/be385d6214ae4c3f8190a49d4ea0cee8.jpg',
            "name": 'Sneaker Wipes',
            "value": '19<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290779,
            "cartid": 'SWZ-001-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-sneaker-wipes.html',
            "text": '<span>The Sneaker LAB Sneaker Wipes</span><span> are designed for general purpose use</span><span> across a variety of materials</span>',
            'bodyclass': 'opt71-small'
        },
        '5': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/02929187734b435d9e6710e5100ef95a.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/941880afb9244faa9eff0c5b7d18e43d.png',
            "img": '//cdn.optimizely.com/img/6092490016/74810f95ea2b4f3aa302d3f5a6353d3b.jpg',
            "name": 'Basic Kit',
            "value": '24<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290775,
            "cartid": 'SP2-001-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-basic-kit.html',
            "text": '<span>Sneaker LAB\'s Basic Kit contains</span><span> the bare essentials including the</span><span> Sneaker Cleaner and Premium Brush.</span>',
            'bodyclass': 'opt71-small'
        },
        '6': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/a19582a024f546548ad1c6fc586f8362.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/f8e3eb32f0ed45b8a16e1606212293bb.png',
            "img": '//cdn.optimizely.com/img/6092490016/5984c7e695f64abba284e5e98b9a408d.jpg',
            "name": 'Micro Fibre Towel',
            "value": '14<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290773,
            "cartid": 'SL-MICRO-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-micro-fibre-towel.html',
            "text": '<span>An absorbent, non-abrasive towel</span><span> for wiping cleaning solution</span><span> and excess water from footwear.</span>',
            'bodyclass': 'opt71-small'
        },
        '7': {
            "brand": 'Sneaker Lab',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/c16ae46f38cc4353b386f422c7e16fa7.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/bd28055c163541fdb8d1479cba78caa4.png',
            "img": '//cdn.optimizely.com/img/6092490016/37add85a993d4119829fc62929964f5e.jpg',
            "name": 'Leather Wipes',
            "value": '19<sup>.99</sup>',
            "sAttr": 4964,
            "pID": 290771,
            "cartid": 'LWZ-001-NOC',
            "href": 'https://www.hypedc.com/sneaker-lab-leather-wipes.html',
            "text": '<span>Sneaker LAB has developed a</span><span> water-based, non-toxic wipe</span><span> optimised for leather-based goods.</span>',
            'bodyclass': 'opt71-small'
        },
        '8': {
            "brand": 'Hype DC',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/b3c390297612405c900b0820f98e8cb9.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/1b5021174f7c4b00b53e8efac93219e5.png',
            "img": '//cdn.optimizely.com/img/6092490016/37add85a993d4119829fc62929964f5e.jpg',
            "name": 'Shoe Care Sneaker Protector (160ml)',
            "value": '34<sup>.99</sup>',
            "sAttr": 5020,
            "pID": 293960,
            "cartid": 'HY-PUMP-NOC',
            "href": 'https://www.hypedc.com/hy-pump-160ml.html',
            "text": '<span>Introducing</span><span> Hype DC line</span><span> sneaker protectors.</span>',
            'bodyclass': 'opt71-small'
        },
        '9': {
            "brand": 'Hype DC',
            "img-desktop": '//cdn.optimizely.com/img/6092490016/17fdf2df96f64847b4943c8a01f3b6d9.png',
            "img-mobile": '//cdn.optimizely.com/img/6092490016/bcedbb56b0384a999d24bc773dd469a6.png',
            "img": '//cdn.optimizely.com/img/6092490016/37add85a993d4119829fc62929964f5e.jpg',
            "name": 'Shoe Care Sneaker Protector (200ml)',
            "value": '19<sup>.99</sup>',
            "sAttr": 5020,
            "pID": 293962,
            "cartid": 'HY-WBPRO-NOC',
            "href": 'https://www.hypedc.com/hy-wbpro-200ml.html',
            "text": '<span>Introducing</span><span> the Hype DC line</span><span> of sneaker protectors.</span>',
            'bodyclass': 'opt71-small'
        }
    };

    function loadopt() {
        jQuery.fn.adjustHeight = function () {
            var maxHeightFound = 0;
            this.css('min-height', '1px');
            this.each(function () {
                if (jQuery(this).outerHeight() > maxHeightFound) {
                    maxHeightFound = jQuery(this).outerHeight();
                }
            });
            this.css('min-height', maxHeightFound);
        };


        var datasize = Object.keys(products).length;
        var dataset = Math.floor(Math.random() * datasize);

        function setproduct() {
            var setvalue = false;

            if (window.location.href == products[dataset]['href']) {
                dataset = Math.floor(Math.random() * datasize);
                setTimeout(function () {
                    setproduct();
                }, 50);
            } else {
                var productcheck = false;
                jQuery('.mini-cart-products .list-group-item').each(function () {
                    var productid = jQuery.parseJSON(jQuery(this).find('.btn-remove-cartitem').attr('data-product'));
                    if (products[dataset]['cartid'] == productid['id']) {
                        productcheck = true;
                        return false;
                    }
                });

                if (productcheck) {
                    dataset = Math.floor(Math.random() * datasize);
                    setTimeout(function () {
                        setproduct();
                    }, 50);
                } else {

                    jQuery('.category-products .item').each(function () {
                        var id = jQuery(this).find('.price-div.product-price .price');
                        if (id.hasClass('product-price')) {
                            id = id.parent().attr('id');
                        } else {
                            id = id.attr('id');
                        }
                        id = id.split('price-')[1];
                        if (id == products[dataset]['pID']) {
                            setvalue = true;
                            return false;
                        }
                    });
                    if (setvalue) {
                        dataset = Math.floor(Math.random() * datasize);
                        setTimeout(function () {
                            setproduct();
                        }, 50);
                    } else {
                        runopt();
                    }
                }
            }
        }

        setproduct();

        function runopt() {
            var getdate = localStorage.getItem('opt72-cross-sell');

            if (getdate) {
                getdate = new Date(getdate);
            }

            var datenow = new Date();
            if (!getdate || datenow > getdate) {
                jQuery('body').addClass('opt72');

                var formKey = jQuery("input[name='form_key']").val();

                var html = '<div class="opt72-group"><button type="button" class="opt72-toggle">X</button><div class="opt72-bg"></div><div class="opt72-img-group"><img class="opt72-desktop" src="' + products[dataset]['img-desktop'] + '"/><img class="opt72-laptop" src="' + products[dataset]['img-mobile'] + '"/></div><div class="opt72-info"><div class="opt72-heading">KEEP IT CLEAN</div><div class="opt72-sub-heading">' + products[dataset]['brand'] + ' ' + products[dataset]['name'] + '</div><div class="opt72-text">' + products[dataset]['text'] + '</div><div class="opt72-price">$' + products[dataset]['value'] + '</div><div class="opt72-add-to-cart"><button type="button" class="opt72-button">ADD TO CART</button></div></div></div>';
                jQuery('#carousel-product .addtocart').after(html);
                jQuery('body').append(html);

                function addProductForm() {
                    var pID = products[dataset]["pID"],
                        fID = formKey,
                        sAttr = products[dataset]["sAttr"],
                        qty = 1;
                    jQuery("body").append('<form action="https://www.hypedc.com/checkout/cart/add/product/' + pID + '/" method="post" id="opt-product-form"> <input name="form_key" type="hidden" value="' + fID + '"><input type="hidden" name="qty" id="qty" value="' + qty + '" title="Qty" class="qty"><input type="hidden" name="product" value="' + pID + '"><input type="hidden" name="super_attribute[609]" value="' + sAttr + '"></form>');
                }

                function submitAddToCart() {
                    var url = jQuery("#opt-product-form").attr("action");
                    jQuery.ajax({
                        type: "POST",
                        url: url,
                        data: jQuery("#opt-product-form").serialize(),
                        success: function () {
                            console.log('success');
                            setTimeout(function () {
                                document.location.reload();
                            }, 50);
                        },
                    });
                }

                function start() {
                    var fID = jQuery("input[name='form_key']").val();
                    var qty = 1;
                }

                jQuery(".opt72-button").click(function () {
                    submitAddToCart();
                });

                jQuery('.opt72-toggle').click(function () {
                    jQuery('body > .opt72-group').remove();
                    var datefive = new Date();
                    datefive.setMinutes(datenow.getMinutes() + 5);
                    localStorage.setItem('opt72-cross-sell', datefive);
                });

                setTimeout(function () {
                    jQuery('body').addClass('opt72-active');
                }, 5000);

                addProductForm();
            }
        }
        jQuery(window).resize(function () {
            jQuery('.opt72-group > div:not(.opt72-bg)').adjustHeight();
        });
        jQuery('.opt72-group > div:not(.opt72-bg)').adjustHeight();
    }


    function defer(method, selector) {
        if (window.jQuery) {
            if (jQuery(selector).length > 0) {
                method();
            } else {
                setTimeout(function () {
                    defer(method, selector);
                }, 50);
            }
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    }

    defer(loadopt, 'footer');
}