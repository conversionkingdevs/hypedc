

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    console.clear();

    jQuery('body').addClass('test82v1');

    jQuery(".test82v1 .navbar .nav-primary").before('<div class="opt-overlay"></div>');
		jQuery("#navbar-item-cat-4931").removeClass("dropdown");
    jQuery(".navbar nav ul.nav-primary > li.dropdown").each(function(){
        jQuery(this).find('.view-all').remove();
        jQuery(this).find('li a').removeAttr('class').removeAttr('data-toggle');
        jQuery(this).find('li a').click(function(e){
            e.preventDefault();
            if(jQuery(this).parent().children().length == 1 || (jQuery(this).parent().hasClass("secondary-item") && jQuery(this).parent().hasClass("expanded"))){
                window.location = jQuery(this).attr('href');
            }
        });
        jQuery(this).find(' > ul ul').append('<li class="secondary-item"><a href="'+ jQuery(this).find('> a').attr('href') +'"><span>View All</span></a>');
    });

    jQuery(".opt-overlay").click(function(){
        jQuery(".navbar-toggle").click();
    });
}, "#navbar-item-brands");