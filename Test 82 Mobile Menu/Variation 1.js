
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    console.clear();
    jQuery('body').addClass('test82v1');
    jQuery(".navbar nav ul.nav-primary li.dropdown").each(function(){
        jQuery(this).find('a').removeAttr('class').removeAttr('data-toggle')
        jQuery(this).find('a').click(function(e){
            e.preventDefault();
            window.location = jQuery(this).attr('href')
        })
    });
    jQuery(".navbar-collapse").click(function(){
        jQuery(".navbar-toggle").click();
    });
    jQuery(".nav-primary").click(function(e){
        e.stopPropagation();
    });

    jQuery("ul.nav-primary.megahover.nav.navbar-nav.navbar-left > li:nth-child(9) a span").after('<small class="glyphicon glyphicon-chevron-right icon-down pull-right text-muted"></small>');
}, "#navbar-item-brands");