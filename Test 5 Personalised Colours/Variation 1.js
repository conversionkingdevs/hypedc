function start () {
        function findActiveSize (el, callback) {
            
            var cart = jQuery(el).closest('.cart-tools'),
                loc = cart.find('.nav-tabs li.active').text().trim(),
                size = cart.find('.size-selector-content li.active').text().trim(),
                ret = loc+' '+size;
    
                
                return ret;
        }  
    
        function updateBrandList () {
            var arr = {};
            jQuery('#modal-Brand-options label.list-group-item').each(function () {
                var name = jQuery(this).text().trim(), 
                    code = jQuery(this).find('input').attr('value');
                
                arr[name] = code;
            });
            return arr;
        }
    
        var buildurl = function (colorC, brandC, typeC, sizeC, genderC) {
            var arr = {"Superga":"2703","adidas":"28","Nike":"55","New Balance":"391","Converse":"30","Nike SB":"478","Lacoste":"32","Asics":"29","Reebok":"330","Puma":"67","Onitsuka Tiger":"36","Supra":"238","TOMS":"63","Birkenstock":"31","Vans":"59"};
            var color = jQuery('#carousel-product').attr('data-colour-id'),
                brand = function () {
                    for (var x = 0; x < dataLayer.length; x++) {
                        if (dataLayer[x].ecommerce && dataLayer[x].ecommerce.detail.products[0].brand){
                        return dataLayer[x].ecommerce.detail.products[0].brand; 
                        }
                    }
                }(),
                size = jQuery('.size-selector-content li.active').attr('data-attributevalueid'),
                gender = jQuery('.breadcrumb li:nth-child(2) a').attr('href').split('?')[0],
                type = jQuery('.breadcrumb li:nth-child(4) a').attr('href').split('?')[0];
    
                brand = arr[brand];
    
    
    
            url = '';
    
            if (typeC == true) {
                url = type;
            } else if (genderC == true){
                url = gender;
            } else {
                url = gender;
            }
            url += '?';
    
            if (colorC == true) {
                url += '&colour='+color;
            }
    
            url += '&isLayerAjax=1';
    
    
            return url;
        };
    
        function geturl (url, callback) { // Gets a url with parameters & will cycle down to no parameters and return a false callback
            console.log(url)
            jQuery.get( url, function( data ) {
                data = data.listing;
                if (jQuery(data).find('.item').length > 0) {
                    if (callback) {
                    callback([true, data]);
                    } else {
                        
                        return data;
                    }
                } else if (jQuery(data).find('.item').length <= 0) {
                    url = url.split('&isLayerAjax=1')[0].split('&');
                    if (url.length < 1) {
                        if (callback) {
                            callback([false]);
                        } else {
                            return false;
                        }
                    } else {
                        url.pop();
                        url = url.join('&') + '&isLayerAjax=1';
                        
                        geturl(url, callback);
                    };
                }
            });
        }
        https://www.hypedc.com/zx-flux-red-red-burgundy.html
        function changeRow(data) {

            var html = jQuery(data[1]),
                row = jQuery(html).find('.category-products.row');
            console.log(html);
            
            var prod = document.location.href.split('?')[0];
            
            jQuery(row).find('div.item a[href="'+prod+'"]').closest('.item').remove();

            jQuery(row).find('div.item:nth-child(n+5)').remove();
            jQuery('.category-products.row').hide();
            jQuery('.category-products.row').html(row)
            

            jQuery('.category-products.row').find('div.item').each(function () {
                jQuery(this).find('img').attr('src', jQuery(this).find('img').attr('data-src'))
            })
            jQuery('.category-products.row .item').hover(function () {
                jQuery(this).find('img').attr('src', jQuery(this).find('img').attr('data-alternate'));
            }, function () {
                jQuery(this).find('img').attr('src', jQuery(this).find('img').attr('data-src'))
            })

            jQuery('.category-products.row').show();
        }
    
        geturl(buildurl(true,true,true,true,true), function (a) { changeRow(a) })


    }

function jQueryDefer(method) {
    if (window.jQuery)
        method();
    else
        setTimeout(function() { jQueryDefer(method) }, 50);
}

jQueryDefer(start)