function start () {

    jQuery('.notify-me').each(function () {
        jQuery(this).clone().insertAfter(jQuery(this)).addClass('optButton');
    });

    window.notify_urls = '#'
    
    jQuery('.notify-me:not(.optButton)').hide();

    function findActiveSize (el, callback) {
        
        var cart = jQuery(el).closest('.cart-tools'),
            loc = cart.find('.nav-tabs li.active').text().trim(),
            size = cart.find('.size-selector-content li.active').text().trim(),
            ret = loc+' '+size;

            
            return ret;
    }

    jQuery('.size-selector-content li[data-stock="out"]').click(function () {
        jQuery('.optButton button').html('<p class="optsmall">Sorry, we don\'t have '+findActiveSize(this)+' in stock.</p><p>SEE SIMILAR SHOES IN MY SIZE</p>')
    });

    function updateBrandList () {
        var arr = {};
        jQuery('#modal-Brand-options label.list-group-item').each(function () {
            var name = jQuery(this).text().trim(), 
                code = jQuery(this).find('input').attr('value');
            
            arr[name] = code;
        });
        return arr;
    }

    var buildurl = function (colorC, brandC, typeC, sizeC, genderC) {
        var arr = {"Superga":"2703","adidas":"28","Nike":"55","New Balance":"391","Converse":"30","Nike SB":"478","Lacoste":"32","Asics":"29","Reebok":"330","Puma":"67","Onitsuka Tiger":"36","Supra":"238","TOMS":"63","Birkenstock":"31","Vans":"59"};
        var color = jQuery('#carousel-product').attr('data-colour-id'),
            brand = function () {
                for (var x = 0; x < dataLayer.length; x++) {
                    if (dataLayer[x].ecommerce && dataLayer[x].ecommerce.detail.products[0].brand){
                    return dataLayer[x].ecommerce.detail.products[0].brand; 
                    }
                }
            }(),
            size = jQuery('.size-selector-content li.active').attr('data-attributevalueid'),
            gender = jQuery('.breadcrumb li:nth-child(2) a').attr('href').split('?')[0],
            type = jQuery('.breadcrumb li:nth-child(4) a').attr('href').split('?')[0];

            brand = arr[brand];



        url = '';

        if (typeC == true) {
            url = type;
        } else if (genderC == true){
            url = gender;
        } else {
            url = gender;
        }
        url += '?';
        if (brandC == true) {
            url += 'manufacturer='+brand;
        }

        if (sizeC == true) {
            url += '&size='+size;
        }

        if (colorC == true) {
            url += '&color='+color;
        }

        url += '&isLayerAjax=1';


        return url;
    };

    function geturl (url, callback) { // Gets a url with parameters & will cycle down to no parameters and return a false callback
        jQuery.get( url, function( data ) {
            data = data.listing;
            if (jQuery(data).find('.item').length > 0) {
                if (callback) {
                callback([true, data]);
                } else {
                    
                    return data;
                }
            } else if (jQuery(data).find('.item').length <= 0) {
                url = url.split('&isLayerAjax=1')[0].split('&');
                if (url.length < 1) {
                    if (callback) {
                        callback([false]);
                    } else {
                        return false;
                    }
                } else {
                    url.pop();
                    url = url.join('&') + '&isLayerAjax=1';
                    
                    geturl(url, callback);
                };
            }
        });
    }

    function showModal(size) {
        var html = ' <div class="optSideBar"> <div class="opt-close">X</div><div class="optSidebarContent"> <div class="optTop"> <div class="optContain34"> <h1>Looking for similar shoes in '+size+'?</h1> <div class="redsplit"></div><p>Check out our staff recommendations:</p></div></div><div class="optBottom"> <div class="loader"> <div class="sk-circle"> <div class="sk-circle1 sk-child"></div><div class="sk-circle2 sk-child"></div><div class="sk-circle3 sk-child"></div><div class="sk-circle4 sk-child"></div><div class="sk-circle5 sk-child"></div><div class="sk-circle6 sk-child"></div><div class="sk-circle7 sk-child"></div><div class="sk-circle8 sk-child"></div><div class="sk-circle9 sk-child"></div><div class="sk-circle10 sk-child"></div><div class="sk-circle11 sk-child"></div><div class="sk-circle12 sk-child"></div></div></div></div></div></div>';
    jQuery('body').append(html);

    }

    function buildSwiper (data, replace) {
        var renderQ = data[0],
            slides = '';

        if (renderQ == true) {
            var count = 0;
            var itemList = jQuery(data[1]).find('.category-products.row');
            for (var x = 0; x < itemList.find('.item').length && x < 10; x++) {
                count++;
                var item = itemList.find('div.item:eq('+x+')'),
                    img = item.find('img').attr('data-src'),
                    brand = item.find('.brand-name').text(),
                    prodname = item.find('.product-name').text(),
                    url = item.find('a:eq(0)').attr('href');

                var slide = '<li> <a class="optTile" href='+url+'> <img src="'+img+'"/> <p class="optBold">'+prodname+'</p><p class="optThin">'+brand+'</p>';
                if (item.find('.special-price').length >= 1) {
                    var oldPrice = item.find('.old-price .price').text(),
                        specPrice = item.find('.special-price .price').text();
                    slide += '<p class="optPrice optSpecial">'+specPric+'</p><p class="oldPrice">'+oldPrice+'</p>';
                } else {
                    var price = item.find('.product-price.price').text(),
                        spcial = 0;
                    slide += '<p class="optPrice">'+price+'</p>';
                }

                slide += '</a></li>';

                slides += slide;

            }

            var html = '<div class="optSwiperContainer"> <h1>'+count+' matches</h1> <div class="flexslider" id="optSwiper"> <ul class="slides"> '+slides+' </ul> </div><div class="optRightBar"></div><div class="optLeftBar"></div>';
            jQuery(replace).html(html);
            jQuery('.opt-close').click(function () {
                jQuery('.optSideBar').remove();
            });
            jQuery('.flexslider#optSwiper').flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                itemWidth: 163,
                itemMargin: 10,
                slideshow: false,
                move: 1,
                direction: "horizontal",
                animation: "slide",
                minItems: 2,
                maxItems: 3,
                prevText: "",
                nextText: "",
                touch: true

            });
        } else {
            jQuery(replace).html('<div class="optSwiperContainer"><h1>Opps, we couldn\'t find any similar products.</h1></div>');
        }
    }

    jQuery('.optButton').click(function (e) {
        var size = findActiveSize(this);
        showModal(size);
        geturl(buildurl(true,true,true,true,true), function (a) {buildSwiper(a, '.optSidebarContent .optBottom'); });
    });
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(start, '.btn-notifyme')