function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {

    if(jQuery(window).width() > 767){
        jQuery('body.catalog-product-view').addClass('opt2-v2');    
    };



    /**************************************
        LAYOUT V2
    **************************************/
    jQuery('<div class="cart-tools cart-tools-custom" />').insertAfter(jQuery(".opt2-v2 .afterpay-installments"));
    jQuery('.cart-tools-custom').append('<div class="custom-size-select-dropdown"><div class="custom-size-select-dropdown-nav"></div><div class="custom-size-select-dropdown-tabs"></div><div>'); 
    jQuery(".opt2-v2 .cart-tools.hidden-xs .addtocart").insertAfter(jQuery(".opt2-v2 .cart-tools-custom"));
    jQuery(".opt2-v2 .cart-tools.hidden-xs .notify-me").insertAfter(jQuery(".opt2-v2 .cart-tools-custom"));

    jQuery(".opt2-v2 #size-selector-desktop-content .tab-pane").each(function(){
        $this = jQuery(this);
        jQuery(".opt2-v2 .custom-size-select-dropdown .custom-size-select-dropdown-tabs").append('<div class="tab-pane">'+$this.html()+'</div>');
    });

    jQuery(".opt2-v2 #size-selector-desktop-tabs li:not(.tab-label)").each(function(){
        $this = jQuery(this);
        jQuery(".opt2-v2 .custom-size-select-dropdown-nav").append($this.html());
    });   

    jQuery('<a href="#" class="toggle-description">Description</a>').insertBefore(jQuery(".opt2-v2 .cart-tools.hidden-xs .product-description"));
    jQuery('body').on('click','.toggle-description',function(e){
        e.preventDefault();
        jQuery(this).stop().toggleClass('closed');
        jQuery(".opt2-v2 .cart-tools.hidden-xs .product-description").stop().slideToggle();
        e.preventDefault();
    });

    jQuery(".opt2-v2 .cart-tools-custom").prepend('<a href="#" class="custom-size-select-button">Select size</a>');

    jQuery('body').on('click','.custom-size-select-button',function(e){
        e.preventDefault();
        e.stopPropagation();
        jQuery(this).stop().toggleClass('closed');
        jQuery(".custom-size-select-dropdown").stop().slideToggle();
    });

    jQuery(".custom-size-select-dropdown-nav a:first-of-type").addClass('active');
    jQuery(".custom-size-select-dropdown-nav a").removeAttr('data-toggle');
    jQuery(".custom-size-select-dropdown-nav a").click(function(e){
        e.preventDefault();
        $this = jQuery(this); 
        $index = $this.index()+1;
        jQuery(".custom-size-select-dropdown-nav a").removeClass('active');
        $this.addClass('active');   
        jQuery(".custom-size-select-dropdown-tabs .tab-pane").removeClass('active');
        jQuery(".custom-size-select-dropdown-tabs .tab-pane:nth-child("+$index+")").addClass('active');
    });

    jQuery(".custom-size-select-dropdown-tabs .tab-pane:first-of-type").addClass('active');

    jQuery("body").on('click','.custom-size-select-dropdown .tab-pane a',function(e){
        $this = jQuery(this);
        jQuery(".custom-size-select-dropdown-tabs .tab-pane li a").removeClass('active');
        $this.addClass('active');
        $id = $this.parent().data('attributevalueid');
        jQuery("#size-selector-desktop-content li[data-attributevalueid="+$id+"] a").click();
        e.preventDefault();
        e.stopPropagation();
        
        jQuery(".custom-size-select-button").stop().removeClass('closed');
        jQuery(".custom-size-select-dropdown").stop().slideUp();
    });



    /***** Carousel *****/
    jQuery(".opt2-v2 #carousel-product").append('<div id="carousel-wrap-container"><div id="carousel-wrap"><div id="carousel" class="flexslider"><ul class="slides"></ul></div></div></div>');

    jQuery("#main-image.flexslider li:not(.clone) img").each(function(){
        $this = jQuery(this);
        jQuery("#carousel .slides").append('<li>'+$this.parent().html()+'</li>')
    })

    jQuery("#carousel").flexslider({
        controlNav: false,
        asNavFor: '#main-image.flexslider',
        animation: "slide",
        animationLoop: true,
        itemWidth: 210,
        itemMargin: 0,
        minItems: 4,
        maxItems: 4,
        move: 1,
        directionNav: true,
        slideshow: false,
        start: function(slider){
            jQuery("#carousel-wrap-container").prependTo($("#carousel-product"));
            jQuery("#carousel-wrap-container").css('bottom',jQuery("#carousel-product").height() - jQuery("#main-image.flexslider").height() - jQuery("#carousel-wrap-container").height() - 50);        
        }
    });


    jQuery(window).resize(function(){
        if(jQuery(window).width() < 1200){
            jQuery("#carousel-wrap-container").css('bottom',jQuery("#carousel-product").height() - jQuery("#main-image.flexslider").height() - jQuery("#carousel-wrap-container").height() - 50);    
        };
    })

}, '.afterpay-installments');
