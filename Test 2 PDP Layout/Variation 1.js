function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
    if(jQuery(window).width() > 767){
        jQuery('body.catalog-product-view').addClass('opt2-v1');    
    };
}, 'body.catalog-product-view');
