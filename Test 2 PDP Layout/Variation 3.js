console.clear();
if(jQuery(window).width() > 767){
    jQuery('body.catalog-product-view').addClass('opt2-v3');    
};



/**************************************
    LAYOUT V3
**************************************/
jQuery('<div class="cart-tools cart-tools-custom" />').insertAfter(jQuery(".opt2-v3 .afterpay-installments"));
jQuery(".opt2-v3 #product-options-wrapper").appendTo('.cart-tools-custom');
jQuery(".opt2-v3 .cart-tools.hidden-xs .addtocart").insertAfter(jQuery(".opt2-v3 .cart-tools-custom"));
jQuery(".opt2-v3 .cart-tools.hidden-xs .notify-me").insertAfter(jQuery(".opt2-v3 .cart-tools-custom"));
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {

    jQuery('<a href="#" class="toggle-description">Description</a>').insertBefore(jQuery(".opt2-v3 .cart-tools.hidden-xs .product-description"));
    jQuery('body').on('click','.toggle-description',function(e){
        e.preventDefault();
        jQuery(this).stop().toggleClass('closed');
        jQuery(".opt2-v3 .cart-tools.hidden-xs .product-description").stop().slideToggle();
        e.preventDefault();
    });



    /***** Carousel *****/
    jQuery("#main-image.flexslider li:not(.clone) img").each(function(){
        $this = jQuery(this);
        jQuery(".opt2-v3 .col-md-14.image-padding").append($this.parent().html());
    });

    jQuery(".opt2-v3 #carousel-product .container .pull-right a").prependTo(jQuery(".opt2-v3 .page-header"));

}, '.product-description');