function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
            setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
      if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
      }
    };
    document.querySelector('head').appendChild(s);
}

var optProducts;

function init() {
    if(window.localStorage.optStorage !== undefined) {
        optProducts = JSON.parse(window.localStorage.optStorage);

        jQuery('body').addClass('opt6A v2');

        jQuery('.opt6A .row.new-arrivals').after('<section class="optWrap"></section>')

        jQuery('.opt6A .optWrap').append('<h2 class="optHead">YOUR RECENTLY VIEWED</h2><div class="swiper-container"><div class="swiper-wrapper"></div></div><div class="swiper-button-next"></div><div class="swiper-button-prev"></div>')

        defer(function(){
            populate();
        }, '.optWrap .swiper-wrapper');
    }
}

function populate() {
  if (Object.keys(optProducts).length == 1) {
     	jQuery('body').addClass('opt-singlerecentlyviewed'); 
    }
    for(var i=0; i < Object.keys(optProducts).length; i++) {
        jQuery('.optWrap .swiper-wrapper').prepend('<div class="swiper-slide"><div class="optPictures"></div><div class="optItemInfo"></div></div>');
     
        jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optPictures').prepend('<img class="shoePic" src="'+optProducts[i].img[1]+'">');

        jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optItemInfo').append('<h2 class="infoHead">'+optProducts[i].name+'</h2>');

        if(optProducts[i].oldprice !== "") {
            jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optItemInfo').append('<h2 class="infoPrice"><span class="infoSlash">'+optProducts[i].oldprice+'</span><span class="infoSale">$'+optProducts[i].price+'</span></h2>');
        } else {
            jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optItemInfo').append('<h2 class="infoPrice">$'+optProducts[i].price+'</h2>');
        }

        jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optItemInfo').append('<p class="infoAfter">OR 4 PAYMENTS OF <var>'+optProducts[i].afterprice+'</var> WITH <span class="afterLogo"><img src="https://static.hypedc.com/skin/frontend/hypedc/default/images/afterpay/afterpay-icon-large.svg" alt="AfterLogo"></span></p>');

        
        jQuery('.optWrap .swiper-wrapper .swiper-slide:eq(0) .optItemInfo').append('<a class="infoBtn" href="'+optProducts[i].link+'"><div>SHOP NOW</div></a>');

    }   
}

defer(function(){
    init();
    console.log('get ze swiper');
    getScript('https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.5/js/swiper.js',function(){
        jQuery('head').append('<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.5/css/swiper.css">');
        console.log('swiper gotted');
        var swiper = new Swiper('.swiper-container', {
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
          });
    });
},'.row.new-arrivals')